//
//  ViewController.swift
//  ERIRPG - Dices
//
//  Created by entelgy on 18/07/2018.
//  Copyright © 2018 ERIMIA. All rights reserved.
//

import UIKit
import Darwin
import GoogleMobileAds
import StoreKit




class ViewController: UIViewController {
    
    var bannerView: GADBannerView!


    @IBOutlet weak var d4: UITextField!
    @IBOutlet weak var d6: UITextField!
    @IBOutlet weak var d8: UITextField!
    @IBOutlet weak var d10: UITextField!
    @IBOutlet weak var d12: UITextField!
    @IBOutlet weak var d20: UITextField!
    
    var count = 6
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "calcular" {
            let vcCalcular = segue.destination as! CalcularViewController
            
            if (d4.text?.isEmpty == false){
                var d4resultValue = 0
                for _ in 1...Int(d4.text!)! {
                    let tempD4Value = Int(arc4random_uniform(4) + 1)
                    d4resultValue += tempD4Value
                    vcCalcular.total += tempD4Value
                    vcCalcular.detalheDics.append("D4: \(tempD4Value)")
                }
                vcCalcular.d4 = d4resultValue
            }
            
            if (d6.text?.isEmpty == false){
                var d6resultValue = 0
                for _ in 1...Int(d6.text!)! {
                    let tempD6Value = Int(arc4random_uniform(6) + 1)
                    d6resultValue += tempD6Value
                    vcCalcular.total += tempD6Value
                    vcCalcular.detalheDics.append("D6: \(tempD6Value)")
                }
                vcCalcular.d6 = d6resultValue
            }
            
            if (d8.text?.isEmpty == false){
                var d8resultValue = 0
                for _ in 1...Int(d8.text!)! {
                    let tempD8Value = Int(arc4random_uniform(8) + 1)
                    d8resultValue += tempD8Value
                    vcCalcular.total += tempD8Value
                    vcCalcular.detalheDics.append("D8: \(tempD8Value)")
                }
                vcCalcular.d8 = d8resultValue
            }
            
            if (d10.text?.isEmpty == false){
                var d10resultValue = 0
                for _ in 1...Int(d10.text!)! {
                    let tempD10Value = Int(arc4random_uniform(10) + 1)
                    d10resultValue += tempD10Value
                    vcCalcular.total += tempD10Value
                    vcCalcular.detalheDics.append("D10: \(tempD10Value)")
                }
                vcCalcular.d10 = d10resultValue
            }
            
            if (d12.text?.isEmpty == false){
                var d12resultValue = 0
                for _ in 1...Int(d12.text!)! {
                    let tempD12Value = Int(arc4random_uniform(12) + 1)
                    d12resultValue += tempD12Value
                    vcCalcular.total += tempD12Value
                    vcCalcular.detalheDics.append("D12: \(tempD12Value)")
                }
                vcCalcular.d12 = d12resultValue
            }
            
            if (d20.text?.isEmpty == false){
                var d20resultValue = 0
                for _ in 1...Int(d20.text!)! {
                    let tempD20Value = Int(arc4random_uniform(20) + 1)
                    d20resultValue += tempD20Value
                    vcCalcular.total += tempD20Value
                    vcCalcular.detalheDics.append("D20: \(tempD20Value)")
                }
                vcCalcular.d20 = d20resultValue
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // In this case, we instantiate the banner with desired ad size.
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = "ca-app-pub-5493383814534386/9920298223"// TEST: "ca-app-pub-3940256099942544/2934735716" PROD: ca-app-pub-5493383814534386/9920298223
        bannerView.rootViewController = self
        
        addBannerViewToView(bannerView)
        bannerView.load(GADRequest())
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //teste
        if count <= 0 {
            if #available( iOS 10.3,*){
                SKStoreReviewController.requestReview()
            }
            count = 5
        } else {
            count = count - 1
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clearDicesInfo(_ sender: Any) {
        self.d4.text = ""
        self.d6.text = ""
        self.d8.text = ""
        self.d10.text = ""
        self.d12.text = ""
        self.d20.text = ""
    }
    
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    
    @IBAction func d4MinusButtonClick(_ sender: Any) {
        if(!(self.d4.text?.isEmpty)!){
            
            let valueD4 : Int = Int(self.d4.text!)!
            if(valueD4 - 1 <= 0){
                self.d4.text = ""
                return
            }
            self.d4.text = String( valueD4 - 1 )
        }
    }
    
    @IBAction func d4PlusButtonClick(_ sender: Any) {
        if(!(self.d4.text?.isEmpty)!){
            let valueD4 : Int = Int(self.d4.text!)!
            self.d4.text = String( valueD4 + 1 )
        }else{
            self.d4.text = String( 1 )
        }
    }
    
    @IBAction func d6MinusButtonClick(_ sender: Any) {
        if(!(self.d6.text?.isEmpty)!){
            
            let valueD6 : Int = Int(self.d6.text!)!
            if(valueD6 - 1 <= 0){
                self.d6.text = ""
                return
            }
            self.d6.text = String( valueD6 - 1 )
        }
    }
    
    @IBAction func d6PlusButtonClick(_ sender: Any) {
        if(!(self.d6.text?.isEmpty)!){
            let valueD6 : Int = Int(self.d6.text!)!
            self.d6.text = String( valueD6 + 1 )
        }else{
            self.d6.text = String( 1 )
        }
    }
    
    @IBAction func d8MinusButtonClick(_ sender: Any) {
        if(!(self.d8.text?.isEmpty)!){
            
            let valueD8 : Int = Int(self.d8.text!)!
            if(valueD8 - 1 <= 0){
                self.d8.text = ""
                return
            }
            self.d8.text = String( valueD8 - 1 )
        }
    }
    
    @IBAction func d8PlusButtonClick(_ sender: Any) {
        if(!(self.d8.text?.isEmpty)!){
            let valueD8 : Int = Int(self.d8.text!)!
            self.d8.text = String( valueD8 + 1 )
        }else{
            self.d8.text = String( 1 )
        }
    }
    
    @IBAction func d10MinusButtonClick(_ sender: Any) {
        if(!(self.d10.text?.isEmpty)!){
            
            let valueD10 : Int = Int(self.d10.text!)!
            if(valueD10 - 1 <= 0){
                self.d10.text = ""
                return
            }
            self.d10.text = String( valueD10 - 1 )
        }
    }
    
    @IBAction func d10PlusButtonClick(_ sender: Any) {
        if(!(self.d10.text?.isEmpty)!){
            let valueD10 : Int = Int(self.d10.text!)!
            self.d10.text = String( valueD10 + 1 )
        }else{
            self.d10.text = String( 1 )
        }
    }
    
    @IBAction func d12MinuButtonClick(_ sender: Any) {
        if(!(self.d12.text?.isEmpty)!){
            
            let valueD12 : Int = Int(self.d12.text!)!
            if(valueD12 - 1 <= 0){
                self.d12.text = ""
                return
            }
            self.d12.text = String( valueD12 - 1 )
        }
    }
    
    @IBAction func d12PlusButtonClick(_ sender: Any) {
        if(!(self.d12.text?.isEmpty)!){
            let valueD12 : Int = Int(self.d12.text!)!
            self.d12.text = String( valueD12 + 1 )
        }else{
            self.d12.text = String( 1 )
        }
    }
    
    @IBAction func d20MinusButtonClick(_ sender: Any) {
        if(!(self.d20.text?.isEmpty)!){
            
            let valueD20 : Int = Int(self.d20.text!)!
            if(valueD20 - 1 <= 0){
                self.d20.text = ""
                return
            }
            self.d20.text = String( valueD20 - 1 )
        }
    }
    
    @IBAction func d20PlusButtonClick(_ sender: Any) {
        if(!(self.d20.text?.isEmpty)!){
            let valueD20 : Int = Int(self.d20.text!)!
            self.d20.text = String( valueD20 + 1 )
        }else{
            self.d20.text = String( 1 )
        }
    }
    
}

