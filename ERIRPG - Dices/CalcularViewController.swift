//
//  CalcularViewController.swift
//  ERIRPG - Dices
//
//  Created by entelgy on 18/07/2018.
//  Copyright © 2018 ERIMIA. All rights reserved.
//

import UIKit
import GoogleMobileAds


class CalcularViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
    var bannerView: GADBannerView!


    var d4 : Int = 0
    var d6 : Int = 0
    var d8 : Int = 0
    var d10 : Int = 0
    var d12 : Int = 0
    var d20 : Int = 0
    var total : Int = 0
    var detalheDics : [String] = []
    
    @IBOutlet weak var TFd4: UILabel!
    @IBOutlet weak var TFd6: UILabel!
    @IBOutlet weak var TFd8: UILabel!
    @IBOutlet weak var TFd10: UILabel!
    @IBOutlet weak var TFd12: UILabel!
    @IBOutlet weak var TFd20: UILabel!
    @IBOutlet weak var TFTotal: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TFd4.text = String(d4)
        TFd6.text = String(d6)
        TFd8.text = String(d8)
        TFd10.text = String(d10)
        TFd12.text = String(d12)
        TFd20.text = String(d20)
        TFTotal.text = String(total)
        
        // In this case, we instantiate the banner with desired ad size.
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = "ca-app-pub-5493383814534386/9920298223"// TEST: "ca-app-pub-3940256099942544/2934735716" PROD: ca-app-pub-5493383814534386/9920298223
        bannerView.rootViewController = self
        
        addBannerViewToView(bannerView)
        bannerView.load(GADRequest())
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //Tabela
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detalheDics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detalheDices", for: indexPath)
        
        let dice = detalheDics[indexPath.row]
        cell.textLabel?.text = dice
        
        return cell
    }
}
